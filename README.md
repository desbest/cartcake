# Cartcake

Cartcake is a **very** simple shopping cart that was made as an experiment. Although it lacks lots of features, I still managed to make hundreds of pounds making websites with this script for clients.

[More information on the Cartcake website](http://desbest.com/cartcake) or on the [gitlab wiki](https://gitlab.com/desbest/cartcake/-/wikis/home).

[Read the changelog here.](https://gitlab.com/desbest/cartcake/-/wikis/changelog)

## Features

* Export your customers to a CSV file
* Email alerts when a product becomes low on stock
* Assign products one image, a description, and a price
* Support for virtual products like file downloads
* Choose to sell in £GBP or $USD
* Variations (so you can sell a M/L/XL t-shirt on the same product page)
* Plugins to extend your shop functionality
* Categories
* Infinite Scrolling
* Mark products as dispatched
* Customers can check when their order is dispatched
* Support for conversions so you can insert your affiliate marketing tracking code
* Themes and colour schemes for themes
* Easily add your analytics code
* Open source so there are zero monthly fees and zero commission

## Payment Processors
* PayPal (PayPal IPN, PayPal Sandbox IPN)
* Moneybookers/Skrill

## I want help

[email desbest](http://desbest.com/contact)

## Missing payment processors
* Stripe payments
* Paymentwall payments

## What features are missing?

* VAT / Sales Tax
* Choosing your time zone
* Recurring payments
* SMTP settings for sending emails
* Custom fields for checkout
* Give customers refunds
* Products with unlimited stock
* Choosing your country
* Multiple currencies
* Account area for customers
* Multiple images per product
* Analytics
* Sales and Vouchers
* Shipping and delivery tracking
* Sub-categories
* Bulk edit products
* Import products from CSV/XML/JSON
* Search for products
* Pretty permalinks
* Custom slugs for products and categories
* Different shipping costs for different countries and everywhere else
* Mark products as "hidden", "sold out" and "coming soon"
* Multiple variations for one product

<?php
require("../config.php");
$settings = mysql_query("SELECT * FROM settings LIMIT 1");
$settings = mysql_fetch_array($settings);
$minimumstock = $settings['minimumstock'];
$themevariation = $settings['themevariation'];

function clean($array) {
    return array_map('mysql_real_escape_string', $array);
}
if(!isset($_POST['description'])){ $_POST = clean($_POST); $_GET = clean($_GET); }

if (!$logged || $logged['level'] != "5"){ exit(); }

function sillyquotes($content){
///////////////////convert curly quotes to straight ones, or the ascii code
//with thanks from http://www.toao.net/48-replacing-smart-quotes-and-em-dashes-in-mysql
////and for the function http://www.snipe.net/2008/12/fixing-curly-quotes-and-em-dashes-in-php/
// First, replace UTF-8 characters.
$content = str_replace(
array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"),
array("'", "'", '&#8220;', '&#8221;', '-', '--', '...'),
$content);
// Next, replace their Windows-1252 equivalents.
$content = str_replace(
array(chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133)),
array("'", "'", '&#8220;', '&#8221;', '-', '--', '...'),
$content);
  return $content;
}

if( isset($_GET['page']) && $_GET['page'] == "pluginsettings"){
    echo "<form method=\"POST\">";
    $fetchsettings = mysql_query("SELECT * FROM pluginsettings WHERE pluginid='$_GET[id]' ");
    
    while ($settings = mysql_fetch_array($fetchsettings)){
        echo "
        <div class='statrow' style='width: 500px;'>
            <div class='statcol' style='width: 100px;'>
            <strong>$settings[attribute]</strong>
            </div>
            <div class='statcol' style='width: 300px;'>
            <input type='text' value='$settings[value]' name='$settings[attribute]' style='width: 250px;'>
            </div>
        </div>
        ";
    }
    echo "<input type='hidden' name='pluginid' value='$_GET[id]'>
    <div class='statrow' style='width: 500px;'><input type='submit' value='Change settings' class='bigbutton' name='pluginsettings'></div>";
    echo "</form>";
}

if(isset($_GET['pageThemeVariation'])){
  $pageThemeVariation = $_GET[pageThemeVariation];
  //echo "pageThemeVariation is $pageThemeVariation"; exit();
  $xmlFileData2 = file_get_contents("../themes/$pageThemeVariation/_config.xml");
  $xmlData2 = new SimpleXMLElement($xmlFileData2);
  foreach($xmlData2->variations->variation as $fetched) {
  if ($fetched->title == $themevariation) { 
  echo "<option value=\"$fetched->title\" style=\"background-color: #98DD98;\">$fetched->title</option>\n"; }
  else { echo "<option value=\"$fetched->title\">$fetched->title</option>\n"; }
  }
}
  
if(isset($_POST['content'])){
  $content=$_POST['content'];
  $datenumber = date("Y-m-d");
  $hour = date('H');
  $minutes = date('i');
  $time = $hour.":".$minutes;
  $datetimenumber = $datenumber."-".$time;
  $seconds = date('s');
  $datetimesecondnumber = $datetimenumber."-".$seconds;
  
  mysql_query("INSERT INTO categories(title, datenumber, datetimenumber, datetimesecondnumber) values ('$content','$datenumber','$datetimenumber','$datetimesecondnumber')");
  $sql_in= mysql_query("SELECT * FROM categories ORDER BY datetimesecondnumber DESC LIMIT 1");
  $r=mysql_fetch_array($sql_in);
  
  echo "
  <div class=\"comment_box\"><b>The category $r[title] has been created.</b>
  <br><a href=\"javascript:location.reload(true)\">Refresh the page</a></div>
  ";
}

if( isset($_POST['itemid2'])){
    $itemid2=$_POST['itemid2'];
    $filename=$_POST['filename'];
    $uploadmode=$_POST['uploadmode'];
    $filename = "uploads/".$filename;
    $checkfor = "../$filename";
    
    if( $uploadmode == 'giveimage'){
        if (file_exists($checkfor)) { mysql_query("UPDATE items SET photopath='$filename' WHERE id='$itemid2'") or die(mysql_error()); }
    }
    if( $uploadmode == 'givefile'){
        if (file_exists($checkfor)) { mysql_query("UPDATE items SET filepath='$filename' WHERE id='$itemid2'") or die(mysql_error()); }
    }

}

if(isset($_POST['newemail']) && isset($_POST['uploading']) && $_POST['uploading'] == "sitelogo"){
    $filename=$_POST['filename'];
    $filename = "uploads/".$filename;
    $checkfor = "../$filename";
    if (file_exists($checkfor)) { mysql_query("UPDATE settings SET sitelogopath='$filename'") or die(mysql_error()); }
}

if(isset($_POST['newemail'])){
  $newemail=$_POST['newemail'];
  
  mysql_query("UPDATE settings SET emailtoalert='$newemail'") or die(mysql_error());    
  echo "
  <div class=\"comment_box2\"><b>Your email preferences have been updated.</b>
  ";
}

if(isset($_POST['itemname'])){
  $itemname=stripslashes($_POST['itemname']);
  $quantity=($_POST['quantity']);
        $allEntities = get_html_translation_table(HTML_ENTITIES, ENT_NOQUOTES);
        $specialEntities = get_html_translation_table(HTML_SPECIALCHARS, ENT_NOQUOTES);
        $newValue = array_diff($allEntities, $specialEntities);
        $newValue = str_replace("&Acirc;","","$newValue");
        //http://www.snipe.net/2008/12/fixing-curly-quotes-and-em-dashes-in-php/
  $categoryid=$_POST['categoryid'];
  
  $datenumber = date("Y-m-d");
  $yearmonthnumber = date("Y-m");
  $hour = date('H');
  $minutes = date('i');
  $time = $hour.":".$minutes;
  $datetimenumber = $datenumber."-".$time;
  $seconds = date('s');
  $datetimesecondnumber = $datetimenumber."-".$seconds;

  mysql_query("
                  INSERT INTO items(title, datenumber, datetimenumber, datetimesecondnumber, yearmonthnumber, categoryid, quantity, mod_datenumber, mod_yearmonthnumber, photopath) 
                  VALUES ('$itemname','$datenumber','$datetimenumber','$datetimesecondnumber','$yearmonthnumber','$categoryid','$quantity','$datenumber','$yearmonthnumber', '')
               ");
                 mysql_query("
                  INSERT INTO history_items(title, datenumber, datetimenumber, datetimesecondnumber, yearmonthnumber, categoryid, quantity, mod_datenumber, mod_yearmonthnumber) 
                  VALUES ('$itemname','$datenumber','$datetimenumber','$datetimesecondnumber','$yearmonthnumber','$categoryid','$quantity','$datenumber','$yearmonthnumber')
               ");
  /*echo "<div class=\"comment_box2\"><b>The item $itemname has been added.</b>
    <br><a href=\"index.php?category=$categoryid\">Refresh the page</a></div>

  "; */
}

if(isset($_POST['itemid'])){

  $itemid=$_POST['itemid'];
    $newvalue = $_POST['newvalue'];
        $allEntities = get_html_translation_table(HTML_ENTITIES, ENT_NOQUOTES);
        $specialEntities = get_html_translation_table(HTML_SPECIALCHARS, ENT_NOQUOTES);
        $newValue = array_diff($allEntities, $specialEntities);
        $newValue = str_replace("&Acirc;","","$newValue");
        //http://www.snipe.net/2008/12/fixing-curly-quotes-and-em-dashes-in-php/
    $datenumber = date("Y-m-d");
    $yearmonthnumber = date("Y-m");

  
  if ($newvalue <= $minimumstock){
      $getsettings = mysql_query("SELECT * FROM settings") or die(mysql_error());
      $getsettings = mysql_fetch_array($getsettings);
      $websiteurl = "$getsettings[websiteurl]";
      $adminurl = $websiteurl."/"."client";

      $to      = "$getsettings[emailtoalert]";
      $subject = 'Stock Manager --> Limited stock';

      $getitem = mysql_query("SELECT * FROM items
      WHERE id='$itemid'") or die(mysql_error());
      $getitem = mysql_fetch_array($getitem);

      $message = "This is Cartcake. \n 
      The item called <b>$getitem[title]</b> has limited stock. \n 
      Use the link below to update the stock. \n
      $adminurl";
      $headers = "From: Cartcake <donotreply@$domainname>" . "\r\n";
      "Reply-To: donotreply@$domainname" . "\r\n" .
      "mime-version: 1.0" . "\r\n" .
      "content-type: text/html; charset=iso-8859-1" . "\r\n";
      "X-Mailer: PHP/" . phpversion();
      @mail($to, $subject, $message, $headers);
   }
   
  mysql_query("UPDATE items SET quantity='$newvalue', mod_datenumber='$datenumber', mod_yearmonthnumber='$yearmonthnumber' WHERE id='$itemid'") or die(mysql_error()); 
}

if(isset($_POST['thatfieldid'])){
  $itemid=$_POST['itemid4'];
  $newvalue=$_POST['newvalue'];
  if ($newvalue == ""){ $newvalue = "0"; }
  $attribute=$_POST['attribute'];
  $thatfieldid=$_POST['thatfieldid'];
   $datenumber = date("Y-m-d");
   $yearmonthnumber = date("Y-m");
   
  mysql_query("UPDATE fields SET value='$newvalue', mod_datenumber='$datenumber', mod_yearmonthnumber='$yearmonthnumber' WHERE id='$thatfieldid'") or die(mysql_error()); 
  
  
  mysql_query("INSERT INTO history_fields
                  (itemid, 
                  attribute, value,
                  datenumber, datetimenumber, datetimesecondnumber, 
                  yearmonthnumber, mod_datenumber, mod_yearmonthnumber) 
                  VALUES (
                  '$itemid',
                  '$attribute','$newvalue',
                  '$datenumber','$datetimenumber','$datetimesecondnumber',
                  '$yearmonthnumber','$datenumber','$yearmonthnumber') 
   ");  
}

if(isset($_POST['daitemid'])){
  $daitemid=$_POST['daitemid'];
  $thecatichoose=$_POST['thecatichoose'];
   $datenumber = date("Y-m-d");
   $yearmonthnumber = date("Y-m");
  mysql_query("UPDATE items SET categoryid='$thecatichoose', mod_datenumber='$datenumber', mod_yearmonthnumber='$yearmonthnumber' WHERE id='$daitemid'") or die(mysql_error()); 
}

if(isset($_POST['description'])){
  $daitemid=$_POST['daitemid2'];
  $description= addslashes(stripslashes(($_POST['description'])));
  //$description= mysql_real_escape_string($_POST[description]);
   $datenumber = date("Y-m-d");
   $yearmonthnumber = date("Y-m");
  mysql_query("UPDATE items SET description='$description', mod_datenumber='$datenumber', mod_yearmonthnumber='$yearmonthnumber' WHERE id='$daitemid'") or die(mysql_error()); 
}

if(isset($_POST['value'])){
  $attribute=$_POST['attribute'];
  $value=$_POST['value'];
  $itemid=$_POST['itemid4'];
  
  $datenumber = date("Y-m-d");
  $yearmonthnumber = date("Y-m");
  $hour = date('H');
  $minutes = date('i');
  $time = $hour.":".$minutes;
  $datetimenumber = $datenumber."-".$time;
  $seconds = date('s');
  $datetimesecondnumber = $datetimenumber."-".$seconds;

  mysql_query("
                  INSERT INTO fields(itemid, datenumber, datetimenumber, datetimesecondnumber, yearmonthnumber, attribute, value, mod_datenumber, mod_yearmonthnumber) 
                  VALUES ('$itemid','$datenumber','$datetimenumber','$datetimesecondnumber','$yearmonthnumber','$attribute','$value','$datenumber','$yearmonthnumber')
               ");
                 mysql_query("
                  INSERT INTO history_fields(itemid, datenumber, datetimenumber, datetimesecondnumber, yearmonthnumber, attribute, value, mod_datenumber, mod_yearmonthnumber) 
                  VALUES ('$itemid','$datenumber','$datetimenumber','$datetimesecondnumber','$yearmonthnumber','$categoryid','$attribute','$value','$yearmonthnumber')
               ");
}

?>
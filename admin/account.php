<?php
include("../config.php");
include("header.php");
?>
<?php
$settings = mysql_query("SELECT * FROM settings LIMIT 1");
$settings = mysql_fetch_array($settings);
$businessname = $settings['businessname'];
$address = $settings['address'];
$phone = $settings['phone'];
$email = $settings['email'];

if ($_POST){
    $businessname = mysql_real_escape_string($_POST['businessname']);
    $address = mysql_real_escape_string($_POST['address']);
    $phone = mysql_real_escape_string($_POST['phone']);
    $email = mysql_real_escape_string($_POST['email']);
    $name = mysql_real_escape_string($_POST['name']);

    $updatesettings = mysql_query("UPDATE settings SET 
    businessname='$businessname', 
    address='$address', 
    phone='$phone', 
    email='$email' 
    ") or die(mysql_error());  

    $notice = "You have updated your account settings.";
}

?>

<div class="boxeshere">
	<div class="menubox">
		<div id="ddblueblockmenu">
		<?php if (!$logged){ ?>
			<div class="menutitle">Actions</div>
			<ul>
			<li><a href="#">Login</a></li>
			<li><a href="#">Help</a></li>
			</ul>
		<?php } ?>
        <?php include("sidemenu.php"); ?>
	</div>
	<div class="contentbox">
		
		<?php
		if (isset($notice)){
	    echo "<div class=\"notice\">
	    $notice
	    </div>"; 
	  	}
		?>
		
		<?php if (!$logged){ ?>
        <font class="headline">Authentication Required</font>
        <br>You have to be logged in to view this page.
        <br>
        <br><a href="index.php">Go to login</a>
		<?php } else {?>
		<font class="headline">Account</font>
		<br>Change your account settings regarding Cartcake.
		<br>
		<form method="POST">
		
		<div class="header" style="400px;">Your Plan</div>
		<div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Hosting Plan: </strong></div>
            <div class="statcol" style="width: 300px;">HL5 @ &pound;2 a month</div>
        </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Cartcake Plan: </strong></div>
            <div class="statcol" style="width: 300px;">TBA</div>
        </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Monthly Charge: </strong></div>
            <div class="statcol" style="width: 300px;">TBA</div>
        </div>



		<div class="header" style="400px;">Your Account</div>
		
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Username: </strong></div>
            <div class="statcol" style="width: 300px;"><?php echo "$logged[username]"; ?></div>
		</div>

        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Password: </strong></div>
            <div class="statcol" style="width: 300px;"><a href="changepass.php">Change my password</a></div>
		</div>
		
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Email: </strong></div>
            <div class="statcol" style="width: 300px;"><?php echo "$logged[email]"; ?></div>
		</div>
		
        <div class="header" style="400px;">Newsletters</div>

        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Cartcake: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="checkbox" checked disabled="disabled"  />
            <br><font style="font-size: 0.8em; margin-bottom: 24px;">You cannot unsubscribe.</font></div>
        </div>
            
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Heroes For Hire: </strong></div>
            <div class="statcol" style="width: 300px;">TBA</div>
        </div>
            
		<div class="header" style="400px;">How We Contact You</div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Your Name: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="name" value="<?php echo "$logged[name]"; ?>"></div>
		</div>
		
        <div style="clear: both;"></div>
		<br>
		The values below are derived from the frontend settings.
		<br>Any changes you make here will appear on your website, affecting your customers.

        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Business Name: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="businessname" value="<?php echo "$businessname"; ?>"></div>
		</div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Address: </strong></div>
            <div class="statcol" style="width: 300px;"><textarea style="width: 250px;" class="mediumtext required" rows="5" name="address"><?php echo "$address"; ?></textarea></div>
		</div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Phone: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="phone" value="<?php echo "$phone"; ?>"></div>
		</div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Email: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="email" value="<?php echo "$email"; ?>"></div>
		</div>
		
		<br><br><br>
		<input type="submit" class="bigbutton" value="Change settings">
		</form>


		<?php } ?>
	</div>
</div>

</body>
</html>
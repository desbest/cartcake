<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Cartcake Admin CP</title>

<link rel="stylesheet" type="text/css" href="stylesheet.css">
<link rel="stylesheet" href="1kbgrid.css" type="text/css">
<link rel="stylesheet" href="cmxform.css" type="text/css">

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/ui-core.js"></script>
<script type="text/javascript" src="js/jeditable.js"></script>
<script type="text/javascript" src="js/editabletext.js"></script>
<script type="text/javascript" src="js/hide.js"></script>
<script type="text/javascript" src="js/validation.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#validatethis").validate();
  });
</script>

</head>
<body>
<div class="headerpane mobile" style="height:147px; background-image: url('graphics/cartcakeheader.png')">
	<div id="navcontainer" class="mobile" style="position: relative; top: 100px;">
    <ul id="navlist">
    <li id="active"><a href="products.php" id="<?=$pcurrent;?>">Products</a></li>
    <li><a href="customers.php?mode=account" id="<?=$ccurrent;?>">Customers</a></li>
    <li><a href="settings.php" id="<?=$scurrent;?>">Settings</a></li>
    </ul>
    </div>
    <?php if (isset($smenu)){ ?>
    	<div id="navcontainer" class="mobile" style="position: relative; top: 100px;">
	    <ul id="navlist">
	    <li id="active"><a href="settings.php" id="<?=$sscurrent;?>">Site Settings</a></li>
	    <li><a href="plugins.php" id="<?=$plcurrent;?>">Plugins</a></li>
	    </ul>
	    </div>
    <?php }
    ?>
</div>	
<div class="headerpane desktop">
<?php

$currentFile = $_SERVER["SCRIPT_NAME"];
$parts = Explode('/', $currentFile);
$currentFile = $parts[count($parts) - 1];

if ($multisite == "yes"){ $multitrail="_client"; } else { $multitrail ="_solo"; }


if (!$logged){ echo "<img src=\"graphics/headerloggedout$multitrail.png\">";  }
elseif ($currentFile == "products.php" || $currentFile == "2products.php" ){ echo " <img src=\"graphics/headerloggedinproducts$multitrail.png\" usemap=\"#imgmap2011223161337\">";  }
elseif ($currentFile == "customers.php"){ echo "<img src=\"graphics/headerloggedincustomers$multitrail.png\" usemap=\"#imgmap2011223161337\">";  } 
elseif ($currentFile == "settings.php" || $currentFile == "plugins.php"){ echo "<img src=\"graphics/headerloggedinsettings$multitrail.png\" usemap=\"#imgmap2011223161337\">";  } 
elseif ($currentFile == "account.php"){ echo "<img src=\"graphics/headerloggedinaccount$multitrail.png\" usemap=\"#imgmap2011223161337\">";  }
elseif ($logged){ echo "<img src=\"graphics/headerloggedin$multitrail.png\" usemap=\"#imgmap2011223161337\">";  }  

?>

<map id="imgmap2011223161337" name="imgmap2011223161337"><area shape="rect" alt="cartcake dashboard" title="" coords="10,77,211,137" href="index.php" target="" /><area shape="rect" alt="products" title="" coords="319,103,409,141" href="products.php" target="" /><area shape="rect" alt="customers" title="" coords="414,102,497,140" href="customers.php" target="" /><area shape="rect" alt="settings" title="" coords="507,104,588,139" href="settings.php" target="" /><area shape="rect" alt="settings" title="" coords="593,104,665,139" href="settings.php" target="" /><!-- Created by Online Image Map Editor (http://www.maschek.hu/imagemap/index) --></map>

</div>
<div class="solution"></div>
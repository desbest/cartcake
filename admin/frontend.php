<?php
include("../config.php");
include("header.php");
?>
<?php
$settings = mysql_query("SELECT * FROM settings LIMIT 1");
$settings = mysql_fetch_array($settings);

$minimumstock = $settings['minimumstock'];
    $emailtoalert = $settings['emailtoalert'];
    $paymentprocessoremail = $settings['paymentprocessoremail'];
    $deliverycost = $settings['deliverycost'];
    $paymentprocessor = $settings['paymentprocessor'];
    $currencysign = $settings['currencysign'];
    $theme = $settings['theme'];
    $themevariation = $settings['themevariation'];
$websiteurl = $settings['websiteurl'];
$businessname = $settings['businessname'];
    $address = $settings['address'];
    $phone = $settings['phone'];
    $email = $settings['email'];
$welcome = stripslashes($settings['welcome']);
    $customhelp = stripslashes($settings['customhelp']);
    $tracking = stripslashes($settings['tracking']);
    $conversions = stripslashes($settings['conversions']);
    
$xmlFileData = file_get_contents("../themes/$theme/_config.xml");
$xmlData = new SimpleXMLElement($xmlFileData);
$logoWidth = $xmlData->logoWidth;
$logoHeight = $xmlData->logoHeight;


if ($_POST){
    $minimumstockP = mysql_real_escape_string($_POST['minimumstock']);
    $emailtoalertP = mysql_real_escape_string($_POST['emailtoalert']);
    $paypalP = mysql_real_escape_string($_POST['paypal']);
    $deliverycostP = mysql_real_escape_string($_POST['deliverycost']);
    $paymentprocessorP = mysql_real_escape_string($_POST['paymentprocessor']);
    $themevariationP = mysql_real_escape_string($_POST['themevariation']);
    $websiteurlP = rtrim(mysql_real_escape_string($_POST['websiteurl']));
    $businessnameP = mysql_real_escape_string($_POST['businessname']);
    $addressP = mysql_real_escape_string($_POST['address']);
    $phoneP = mysql_real_escape_string($_POST['phone']);
    $emailP = mysql_real_escape_string($_POST['email']);
    $welcomeP = mysql_real_escape_string(stripslashes($_POST['welcome']));
    $customhelpP = mysql_real_escape_string(stripslashes($_POST['customhelp']));
    $trackingP = mysql_real_escape_string(stripslashes($_POST['tracking']));
    $conversionsP = mysql_real_escape_string(stripslashes($_POST['conversions']));

    $updatesettings = mysql_query("UPDATE settings SET 
    minimumstock='$minimumstockP', businessname='$businessnameP', 
    emailtoalert='$emailtoalertP', address='$addressP', 
    paypal='$paypalP', phone='$phoneP', paymentprocessor='$paymentprocessorP', 
    themevariation='$themevariationP',
    websiteurl='$websiteurlP', email='$emailP',
    welcome='$welcomeP',customhelp='$customhelpP', tracking='$trackingP', conversions='$conversionsP'
    ") or die(mysql_error());  

    $notice = "You have updated your site's settings.";
}
?>

<link rel="stylesheet" href="fileuploader/client/fileuploader.css" type="text/css">
<script src="fileuploader/client/fileuploader.js" type="text/javascript"></script>
<!-- .giveimage comes first. #gaveimage came after -->

<script type="text/javascript">    
    function createUploader(){    
        var uploader = new qq.FileUploader({
            //var itemid = $("#gaveimage").attr("itemid");
            element: document.getElementById('gaveimage'),
            action: 'fileuploader/server/php.php',
            //action: 'fileuploader/client/do-nothing.htm',
                // ^^ (1/2) check "desbest edit" for where the file saves
            params: {
                uploading: 'sitelogo'
            },            
            allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
            debug: true,
            onSubmit: function(id, fileName){
            },
            onProgress: function(id, fileName, loaded, total){},
            onComplete: function(id, fileName, responseJSON){
                var uploading = "sitelogo";
                var dataString = "uploading=" + uploading + "&filename=" + fileName; //alert(dataString);
                
                $.ajax({
                    type: "POST",
                    url: "ajaxcontrols.php",
                    // ^^ (2/2) check -->$_POST['uploading'] == "sitelogo" 
                    //for the database update
                    data: dataString,
                    cache: false,
                    success: function(html){
                        //$("#gaveimage").slideUp(1000);
                        //$("#gaveimage:after").append("You've replaced the image. <a href=\"#\" onClick=\"history.go()\"> Refresh the page.</a>");
                    }
                });
            
            },
            onCancel: function(id, fileName){
                $("#gaveimage:after").append("There has been an error in updating the product's image. <a href=\"#\" onClick=\"history.go()\">Refresh the page.</a>");
            },
        });           
    }
    
    // in your app create uploader as soon as the DOM is ready
    // don't wait for the window to load  
    window.onload = createUploader;     
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#theme').change(function() {
      var ichose = $(this).val();
      var lookfor = 'ajaxcontrols.php?pageThemeVariation='+ichose;
      //alert (lookfor);
      //case "chapter":        
        $('#themevariation').load('ajaxcontrols.php?pageThemeVariation='+ichose);
      //break;
    }); 
  });
</script>


<div class="boxeshere">
  <div class="menubox desktop">
    <div id="ddblueblockmenu">
    <?php if (!$logged){ ?>
      <div class="menutitle">Actions</div>
      <ul>
      <li><a href="#">Login</a></li>
      <li><a href="#">Help</a></li>
      </ul>
    <?php } ?>
        <?php include("sidemenu.php"); ?>
  </div>
  <div class="contentbox">
    
    <?php
    if (isset($notice)){
      echo "<div class=\"notice\">
      $notice
      </div>"; 
      }
    ?>
    
    <?php if (!$logged){ ?>
        <font class="headline">Authentication Required</font>
        <br>You have to be logged in to view this page.
        <br>
        <br><a href="index.php">Go to login</a>
    <?php } else {?>
    <font class="headline">Frontend</font>
    <br>Change the settings for your website.
    <br>
    <form method="POST">
    
    <div class="header" style="400px;">Miscellaneous</div>
    <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Stock Warning Level: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="minimumstock" value="<?php echo "$minimumstock"; ?>"></div>
        </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Email to alert: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="emailtoalert" value="<?php echo "$emailtoalert"; ?>"></div>
    </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Currency: </strong></div>
            <div class="statcol" style="width: 300px;">
            <select style="width: 250px;" class="required" name="currencysign2" disabled="disabled">
                <option value="<?php echo "$currencysign"; ?>"><?php echo "$currencysign"; ?> GBP Great British Pound</option>
            </select>
            </div>
    </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Selling To: </strong></div>
            <div class="statcol" style="width: 300px;">United Kingdom</div>
    </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>PayPal Email: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="paypal" value="<?php echo "$paymentprocessoremail"; ?>"></div>
    </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>UK Delivery: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="paypal" value="<?php echo "$currencysign"."$deliverycost"; ?>" readonly disabled="disabled">
                <br><font style="font-size: 0.8em; margin-bottom: 12px;">Using Royal Mail 1st class</font></div>
    </div>
    <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Payment Processor: </strong></div>
            <div class="statcol" style="width: 300px;">            
            <select style="width: 250px;" class="required" name="paymentprocessor">
                <option value="<?php echo "$paymentprocessor"; ?>"><?php echo "$paymentprocessor"; ?></option>
                <option value="paypal">Paypal</option>
                <option value="paypalsandbox">Paypal Sandbox</option>
            </select>
            </div>
    </div>

    
        <div class="header" style="400px; clear: both;">Design</div>
        
        <div class="statrow" style="400px;">
    You will be emailed when new themes and variations appear.
    <br>To request a custom design, contact Heroes For Hire.
    </div>

        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Theme: </strong></div>
            <div class="statcol" style="width: 300px;">            
            <select style="width: 250px;" class="required" name="theme" id="theme">
                
                <?php
                foreach(glob('../themes/*', GLOB_ONLYDIR) as $dir) {
                $foldername = explode("/", $dir);
                if ($theme == $foldername[2]) {echo '<option value='.$foldername[2].' selected style="background-color: #98DD98;">'.$foldername[2].'</option>'; }
                else { echo '<option value='.$foldername[2].'>'.$foldername[2].'</option>'; }
                }              
                /* if ($fetched->title == $themevariation) { 
                echo "<option value=\"$theme\">$theme</option>"; }
                else { echo "<option value=\"$theme\">$theme</option>"; } */
                //http://www.liamdelahunty.com/tips/php_list_a_directory.php
                //http://www.webdeveloper.com/forum/showthread.php?t=223634
                ?>
            </select>
            </div>
    </div>
       <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Theme Variation: </strong></div>
            <div class="statcol" style="width: 300px;">            
            <select style="width: 250px;" class="required" name="themevariation" id="themevariation">
              <?php
              foreach($xmlData->variations->variation as $fetched) {
              if ($fetched->title == $themevariation) { 
              echo "<option value=\"$fetched->title\" style=\"background-color: #98DD98;\" selected>$fetched->title</option>"; }
              else { echo "<option value=\"$fetched->title\">$fetched->title</option>"; }
              }
              ?>
            </select>
            </div>
    </div>
    <div class="statrow" style="400px;">
        <div class="statcol" style="width: 100px;"><strong>Logo: </strong></div>
            <div class="statcol" style="width: 300px;"> 
                <div id="gaveimage"><noscript>Javascript is required.</noscript></div>
                <font style="font-size: 0.8em; margin-bottom: 12px;"><?php echo "Upload a $logoWidth"."x"."$logoHeight"."px image for best results."; ?></font></div>
                
        </div>
        
        <div class="header" style="400px; clear: both;">Website Location</div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>URL: </strong>
            <br><font style="font-size: 0.8em; margin-bottom: 12px;">Website address.</font></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="websiteurl" value="<?php echo "$websiteurl"; ?>">
            <br><font style="font-size: 0.8em; margin-bottom: 12px;">Don't forget the http://</font>
            </div>
    </div>
    
        <!-- <div class="statrow" style="500px;">
            <div class="statcol" style="width: 100px;"><strong>Subdomain: </strong></div>
            <div class="statcol" style="width: 400px;">
            <input type="text" style="width: 60px; background-color: #FFFFFF; border: 0px;"  value="http://" readonly>
            <input type="text" style="width: 140px; background-color: #FFFFFF;" name="minimumstock" value="<?php echo "$subdomain"; ?>" maxlength="12" readonly>
            <input type="text" style="width: 150px; background-color: #FFFFFF; border: 0px;"  value="heroesdesign.net" readonly>
            <br><font style="font-size: 0.8em; margin-bottom: 12px;">Cartcake Multisite is not installed. <br>Therefore, multiple shopping carts, mapped to subdomains cannot happen.
            </font>
            </div>
    </div> -->

        
    <div class="header" style="400px; clear: both;">Contact Details</div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Business Name: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="businessname" value="<?php echo "$businessname"; ?>"></div>
    </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Address: </strong></div>
            <div class="statcol" style="width: 300px;"><textarea style="width: 250px;" class="mediumtext required" rows="5" name="address"><?php echo "$address"; ?></textarea></div>
    </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Phone: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="phone" value="<?php echo "$phone"; ?>"></div>
    </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Email: </strong></div>
            <div class="statcol" style="width: 300px;"><input type="text" style="width: 250px;" class="required" name="email" value="<?php echo "$email"; ?>"></div>
    </div>
    
       <div class="header" style="400px; clear: both;">Custom Content</div>
       <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Welcome: </strong></div>
            <div class="statcol" style="width: 300px;"><textarea style="width: 250px;" class="mediumtext required" rows="5" name="welcome"><?php echo "$welcome"; ?></textarea>
             <br><font style="font-size: 0.8em; margin-bottom: 12px;">Displayed on the home page. Plain text only.</font></div>
    </div>
        
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Help page: </strong></div>
            <div class="statcol" style="width: 300px;"><textarea style="width: 250px;" class="mediumtext required" rows="5" name="customhelp"><?php echo "$customhelp"; ?></textarea>
            <br><font style="font-size: 0.8em; margin-bottom: 12px;">Good for entering sizing guides and the return policy.</font></div>
    </div>
        <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Tracking: </strong></div>
            <div class="statcol" style="width: 300px;"><textarea style="width: 250px; font-size: 0.8em;" class="mediumtext required" rows="5" name="tracking"><?php echo "$tracking"; ?></textarea>
            <br><font style="font-size: 0.8em; margin-bottom: 12px;">HTML code before the &lt;/body&gt; tag, good for tracking.</font>
            </div>
    </div>
    <div class="statrow" style="400px;">
            <div class="statcol" style="width: 100px;"><strong>Conversions: </strong></div>
            <div class="statcol" style="width: 300px;"><textarea style="width: 250px; font-size: 0.8em;" class="mediumtext required" rows="5" name="conversions"><?php echo "$conversions"; ?></textarea>
            <br><font style="font-size: 0.8em; margin-bottom: 12px;">HTML code to be executed once a successful order is made.</font>
            <font style="font-size: 0.9em; margin-bottom: 12px;">
            <br>Place <u>&#36;orderid</u> where the order id goes.
            <br>Place <u>&#36;orderamount</u> where the amound paid goes.</font>
            </div>
    </div>


    
    <br><br><br>
    <br><br><br>
    <br><br><br>
    <input type="submit" class="bigbutton" value="Change settings">
    </form>


    <?php } ?>
  </div>
</div>

</body>
</html>
<?php
session_start();
require ("config.php");
require ("functions/tbs_class.php");
/* ------ includes end ------ */

$settings = mysql_query("SELECT * FROM settings LIMIT 1");
$settings = mysql_fetch_array($settings);
$theme = $settings[theme];
$themevariation = $settings[themevariation];
$currencysign = $settings[currencysign];
$tracking = $settings[tracking];
$businessname = $settings[businessname];
$sitelogopath = $settings[sitelogopath];
/* ------ settings end ------ */

$pagemode = "basket";
$basketmode = "ordercomplete";
/* ------ pagemode end ------ */

$orderid = $_SESSION['orderid'];	
$paymentprocessor = $settings[paymentprocessor];
$paymentfunction = "functions/pay_".$paymentprocessor.".php";
require("$paymentfunction");
/* ------ variables end ------ */

if ($payment == "success"){ session_destroy(); }

$TBS = new clsTinyButStrong;
$TBS->LoadTemplate("themes/$theme/ordercomplete.html");
$TBS->MergeBlock('categories',$conn,'SELECT * FROM categories ORDER BY title ASC');
$TBS->Show();
?>
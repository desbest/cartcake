<?php
require("../../config.php");
require("../../functions/hooks.php"); $Hooks = new Hooks;

/* Contents
    ----> Products            
*/

////----> Products
if (isset($_GET['page']) && $_GET['page'] == "products"){
$show =<<<EOF
    <div class="statcol" style="">
       <div class="polaroid"><a href="product.php?id=[product.id]" title="[product.title]"><img src="[product.photopath;ifempty='themes/[var.theme]/graphics/noimg-l.png']" alt="[product.title]"></a><div style="max-width: 150px;">[product.title]</div></div>
    </div>
EOF;

$offset = $_GET['offset'];
$show = $Hooks->FetchProducts($show, $offset);
}
?>
-- phpMyAdmin SQL Dump
-- version 3.3.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 29, 2011 at 05:47 AM
-- Server version: 5.1.44
-- PHP Version: 5.2.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `cartcakev1`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `parentid` varchar(255) NOT NULL DEFAULT '0',
  `items` varchar(255) NOT NULL DEFAULT '0',
  `datenumber` varchar(255) NOT NULL,
  `datetimenumber` varchar(255) NOT NULL,
  `datetimesecondnumber` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `categories`
--

-- --------------------------------------------------------

--
-- Table structure for table `faillogs`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `datenumber` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `faillogs`
--

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE IF NOT EXISTS `fields` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `itemid` int(255) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  `datetimenumber` varchar(255) NOT NULL,
  `datetimesecondnumber` varchar(255) NOT NULL,
  `yearmonthnumber` varchar(255) NOT NULL,
  `mod_datenumber` varchar(255) NOT NULL,
  `mod_yearmonthnumber` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `fields`
--


-- --------------------------------------------------------

--
-- Table structure for table `history_fields`
--

CREATE TABLE IF NOT EXISTS `history_fields` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `itemid` int(255) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  `datetimenumber` varchar(255) NOT NULL,
  `datetimesecondnumber` varchar(255) NOT NULL,
  `yearmonthnumber` varchar(255) NOT NULL,
  `mod_datenumber` varchar(255) NOT NULL,
  `mod_yearmonthnumber` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `history_fields`
--


-- --------------------------------------------------------

--
-- Table structure for table `history_items`
--

CREATE TABLE IF NOT EXISTS `history_items` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `categoryid` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  `datetimenumber` varchar(255) NOT NULL,
  `datetimesecondnumber` varchar(255) NOT NULL,
  `yearmonthnumber` varchar(255) NOT NULL,
  `mod_datenumber` varchar(255) NOT NULL,
  `mod_yearmonthnumber` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `photopath` varchar(255) NOT NULL,
  `filepath` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `history_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `categoryid` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  `datetimenumber` varchar(255) NOT NULL,
  `datetimesecondnumber` varchar(255) NOT NULL,
  `yearmonthnumber` varchar(255) NOT NULL,
  `mod_datenumber` varchar(255) NOT NULL,
  `mod_yearmonthnumber` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `photopath` varchar(255) NOT NULL,
  `filepath` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `items`
--


-- --------------------------------------------------------

--
-- Table structure for table `ordered`
--

CREATE TABLE IF NOT EXISTS `ordered` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `variationid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ordered`
--


-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amountpaid` varchar(255) NOT NULL,
  `currencysign` varchar(255) NOT NULL,
  `currencycode` varchar(255) NOT NULL,
  `orderedid` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  `datetimenumber` varchar(255) NOT NULL,
  `emailsubject` varchar(255) NOT NULL,
  `emailcontent` mediumtext NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_name` varchar(255) NOT NULL,
  `address_street` varchar(255) NOT NULL,
  `address_city` varchar(255) NOT NULL,
  `address_zip` varchar(255) NOT NULL,
  `address_country` varchar(255) NOT NULL,
  `payer_email` varchar(255) NOT NULL,
  `phonenumber` varchar(255) NOT NULL,
  `secondline` varchar(255) NOT NULL,
  `custname` varchar(255) NOT NULL,
  `delivery` varchar(255) NOT NULL,
  `dispatched` varchar(255) NOT NULL DEFAULT 'no',
  `status` varchar(255) NOT NULL DEFAULT 'processing',
  `secret` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `orders`
--

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) NOT NULL,
  `active` varchar(3) NOT NULL DEFAULT 'yes',
  `hook` varchar(255) NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `pluginsettings`
--

CREATE TABLE IF NOT EXISTS `pluginsettings` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pluginid` varchar(255) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pluginstorage`
--

CREATE TABLE IF NOT EXISTS `pluginstorage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginid` int(11) NOT NULL,
  `xtable` varchar(255) NOT NULL,
  `xid` int(11) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `resets`
--

CREATE TABLE IF NOT EXISTS `resets` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `userid` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  `accesscode` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `resets`
--


-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `minimumstock` varchar(255) NOT NULL,
  `emailtoalert` varchar(255) NOT NULL,
  `theme` varchar(255) NOT NULL DEFAULT 'panorama',
  `themevariation` varchar(255) NOT NULL DEFAULT 'default',
  `sitelogopath` varchar(255) NOT NULL,
  `businessname` varchar(255) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `websiteurl` varchar(255) NOT NULL,
  `currencysign` varchar(255) NOT NULL DEFAULT '&pound;',
  `currencycode` varchar(255) NOT NULL DEFAULT 'GBP',
  `paymentprocessoremail` varchar(255) NOT NULL,
  `deliverycost` varchar(255) NOT NULL DEFAULT '5',
  `paymentprocessor` varchar(255) NOT NULL,
  `welcome` mediumtext NOT NULL,
  `customhelp` text NOT NULL,
  `tracking` text NOT NULL,
  `conversions` text NOT NULL,
  `homesort` varchar(255) NOT NULL DEFAULT 'recent',
  `homelist` varchar(255) NOT NULL DEFAULT '6',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`minimumstock`, `theme`, `themevariation`) 
VALUES ('5', 'panorama', 'black');
-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `users`
--


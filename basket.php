 <?php
require ("config.php");
require ("functions/tbs_plugin_aggregate.php");
require ("functions/tbs_class.php");
/* ------ includes end ------ */

$settings = mysql_query("SELECT * FROM settings LIMIT 1");
$settings = mysql_fetch_array($settings);
$theme = $settings['theme'];
$themevariation = $settings['themevariation'];
$currencysign = $settings['currencysign'];
$tracking = $settings['tracking'];
$businessname = $settings['businessname'];
$sitelogopath = $settings['sitelogopath'];
$welcome = $settings['welcome'];

if (isset($_SESSION['basket']['items'])) { $basketitem = $_SESSION['basket']['items']; } else { $basketitem = NULL; }
$deliverycost = $settings['deliverycost'];
if ( !empty($basketitem) && !recursiveArraySearch($basketitem, 'tangible')) { $deliverycost = 'n/a'; }

session_destroy();
/* ------ settings end ------ */

/* ------ deletion before everything else ----- */

if (isset($_GET['removeitem']) && $_GET['removevariation'] != ""){
    $lookfor_item = "i".$_GET['removeitem']; $lookfor_variation = "v".$_GET['removevariation'];
    
    if ( recursiveArraySearch($basketitem, $lookfor_item) || recursiveArraySearch($basketitem, $lookfor_variation) ) {
        //echo "The $lookfor_item item and $lookfor_variation variation is in the array.<br>"; 
        $key_item = recursiveArraySearch($basketitem, $lookfor_item); 
        $key_variation = recursiveArraySearch($basketitem, $lookfor_variation); 
        //echo "The array keys are <b>$key_item</b> for the item and <b>$key_variation</b> for the variation.<br>";
        //print_r($basketitem); exit();
        
        /* ---------> */ unset($_SESSION['basket']['items'][$key_item]);
                           //print_r($_SESSION['basket']['items'][$key_item]);
    }                      $basketitem = $_SESSION['basket']['items'];
} 


/* ----- _____________ ----- */
$pagemode = "basket";
$basketmode = "basket";

if (isset($_SESSION['basket']['items'])) { $basketitem = $_SESSION['basket']['items']; } else { $basketitem = NULL; }//print_r($basketitem);
$basketcount = count($basketitem);
$paymentprocessoremail = $settings['paymentprocessoremail'];
$currencycode = $settings['currencycode'];
$websiteurl = $settings['websiteurl'];
$paymentprocessor = $settings['paymentprocessor'];
$paymentfunction = "functions/pay_".$paymentprocessor.".html";

//session_destroy();
//print_r($basketitem); exit();
/* ------ variables end ------ */

//loop through all the basket entries
// print_r($basketitem); exit();
//echo "$basketcount"; //exit();
if ($basketcount >0){
  for ($i = 1; $i <= $basketcount; $i++) {
      $cting = "c".$i;

      // echo "<br>cting is c$i<br>";
      // print_r($basketitem[$cting]); exit();

      if ($basketitem[$cting] != null) { foreach( $basketitem[$cting] as $key => $value){

        $itemid = $basketitem[$cting]['itemid'];
        $quantity = $basketitem[$cting]['quantity'];
      
        if ($key == "itemid"){
          $fetchproduct = mysql_query("SELECT * FROM items WHERE id ='$itemid'  ");
          $fetchproduct = mysql_fetch_array($fetchproduct);
          $price = $fetchproduct['quantity'];
          $total = $price * $quantity;
          
          $iminusone = $i -1;
          //$basketprices = array();
          $basketprices[$iminusone] = $total;
        
          //echo "Key: $key, Value: $value <br />";
          //echo "Price is $price // Total is $total // i is $i // iminusone is $iminusone<br>";
        }
      } }
      //$grandtotal = $total + $total;
      //echo "Grand total is $grandtotal<br>";
      //echo "<hr>";
  }
  $grandtotal = @array_sum($basketprices) + $deliverycost;
  //print_r($basketprices); exit();
}
/* ------ variables end ------ */

function rec_in_array($needle, $haystack, $alsokeys=false) //do not use. does not work.
    {
        if(!is_array($haystack)) return false;
        if(in_array($needle, $haystack) || ($alsokeys && in_array($needle, array_keys($haystack)) )) return true;
        else {
            foreach($haystack AS $element) {
                $ret = rec_in_array($needle, $element, $alsokeys);
            }
        }
       
        return $ret;
} //http://www.php.net/manual/en/function.in-array.php#58560

function recursiveArraySearch($haystack, $needle, $index = null){
    $aIt     = new RecursiveArrayIterator($haystack);
    $it    = new RecursiveIteratorIterator($aIt);
   
    while($it->valid()){       
        if (((isset($index) AND ($it->key() == $index)) OR (!isset($index))) AND ($it->current() == $needle)) {
            return $aIt->key();
        }
        $it->next();
    }
    return false;
} //http://www.php.net/manual/en/function.array-search.php#97645 
/* ------ basket/array functions end ------ */

if ( isset($_GET['onemoreitem']) && isset($_GET['onemorevariation']) ){
    $lookfor_item = "i".$_GET['onemoreitem']; $lookfor_variation = "v".$_GET['onemorevariation'];
    //echo "i am in!";
    
    if ( recursiveArraySearch($basketitem, $lookfor_item) || recursiveArraySearch($basketitem, $lookfor_variation) ) {
        $key_item = recursiveArraySearch($basketitem, $lookfor_item); 
        $key_variation = recursiveArraySearch($basketitem, $lookfor_variation);
		/* ---------> */ $_SESSION['basket']['items'][$key_variation]['quantity'] = $_SESSION['basket']['items'][$key_variation]['quantity'] +1; 
    //echo "<meta http-equiv=\"refresh\" content=\"0;url=basket.php\" />";

		/* echo "The $lookfor_item item and $lookfor_variation variation is in the array.<br>";
        echo "The array keys are <b>$key_item</b> for the item and <b>$key_variation</b> for the variation.<br>"; */
        //print_r($basketitem); exit();
        
    }
}

if ( isset($_GET['onelessitem']) || isset($_GET['onelessvariation']) ){
    $lookfor_item = "i".$_GET['onelessitem']; $lookfor_variation = "v".$_GET['onelessvariation'];
    if ( recursiveArraySearch($basketitem, $lookfor_item) || recursiveArraySearch($basketitem, $lookfor_variation) ) {
        $key_item = recursiveArraySearch($basketitem, $lookfor_item); 
        $key_variation = recursiveArraySearch($basketitem, $lookfor_variation); 
        /* ---------> */	if ($_SESSION['basket']['items'][$key_variation]['quantity'] >1) {$_SESSION['basket']['items'][$key_variation]['quantity'] = $_SESSION['basket']['items'][$key_item]['quantity'] -1;  
        //echo "<meta http-equiv=\"refresh\" content=\"0;url=basket.php\" />";
        
		/* echo "The $lookfor_item item and $lookfor_variation variation is in the array.<br>";
		echo "The array keys are <b>$key_item</b> for the item and <b>$key_variation</b> for the variation.<br>";
        print_r($basketitem); exit(); */
        }
    }
}

/* ------ basket actions ------ */

$TBS = new clsTinyButStrong;
$TBS->LoadTemplate("themes/$theme/basket.html");
$TBS->MergeBlock('categories',$conn,'SELECT * FROM categories ORDER BY title ASC');
if ($basketcount >0){ $TBS->MergeBlock('basketitem','array',$basketitem); }
$TBS->MergeBlock("ordered",$conn,"SELECT * FROM ordered WHERE id='%p1%' ORDER BY title ASC");
$TBS->MergeBlock("product",$conn,"SELECT * FROM items WHERE id='%p1%' ORDER BY title ASC");
$TBS->MergeBlock("variation",$conn,"SELECT * FROM fields WHERE id='%p1%'");
$TBS->Show();
?>
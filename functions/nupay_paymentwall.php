<?php
// Paymentwall PHP Library: https://www.paymentwall.com/lib/php
require_once('paymentwall.php');
Paymentwall_Config::getInstance()->set(array(
    'api_type' => Paymentwall_Config::API_GOODS,
    'public_key' => '1c41b58203e5f760ceadcecbfbceb198',
    'private_key' => '71a707dc8358121d29a5755e05ee3c1b'
));

$widget = new Paymentwall_Widget(
    "$custid",
    'p10',
    array(
        new Paymentwall_Product(
            "$packageid",                           
            "$amount",
            'USD',                                  
            "$showname",                      
            Paymentwall_Product::TYPE_FIXED
        )
    ),
    array('email' => "$email", 'orderid' => "$orderid", 'grossamount' => "$amount", 'customer[username]' => "$_SESSION[username]", 'customer[address]' => "$_SESSION[address]", 'ag_type' => "fixed", 'success_redirect_url' => "http://hostingz.org/order/complete.php?status=success")
    );
echo $widget->getHtmlCode();
?>
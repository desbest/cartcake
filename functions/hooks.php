<?php

class Hooks {
    /* Contents
        ----> FetchProducts
        ----> ExecuteReduceStock
        ----> ExecuteProcessOrder
        ----> ExecuteLowStock            
        ----> BeforeDownload            
        ----> AfterDownload            
    */

    function EmailFailedOrder($orderid, $paymentprocessor, $reason){
        $settings = mysql_query("SELECT * FROM settings LIMIT 1");
        $settings = mysql_fetch_array($settings);
        $websiteurl = $settings['websiteurl'];
        $domainname = str_replace("http://", "", "$websiteurl");
        $domainchunks = explode("/", $domainname);
        $domainname = $domainchunks[0];
        $sitename = $settings['businessname'];
        $conversions = $settings['conversions'];
        $minimumstock = $settings['minimumstock'];
        $email = $settings[email];
        $sendas = "donotreply@$domainname";

        $subject = $order['emailsubject'];
        $emailcontent = "The order $orderid made with the $paymentprocessor payment processor could not complete for this reason $reason";
        $headers      = "From: $sitename <$sendas>" . "\r\n";      
          $headers .= 'MIME-Version: 1.0' . "\r\n";      
          $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        $emailtask = mail($custemail, $subject, $emailcontent, $headers);
    }
    
    function FetchProducts($show){
        //fetching
        $settings = mysql_query("SELECT * FROM settings LIMIT 1");
        $settings = mysql_fetch_array($settings);
        $homesort = $settings['homesort']; 
        $homelist = $settings['homelist'];
        $theme = $settings['theme'];
        if (isset($_GET['offset'])){ $offset = $_GET['offset']; } else { $offset = 0; }
        if ($homesort == "recent"){
            $fetchproducts = mysql_query("SELECT * FROM items ORDER BY datenumber DESC LIMIT $homelist OFFSET $offset ");
        } elseif($homesort == "alphabetised") {
            $fetchproducts = mysql_query("SELECT * FROM items ORDER BY title ASC LIMIT $homelist OFFSET $offset ");
        } 
        
        //parsing
        $dirtyshow = $show;
        
        while ($product = mysql_fetch_array($fetchproducts)){
            $show = $dirtyshow;
            $show = str_replace("[product.id]",$product['id'],$show);
            $show = str_replace("[product.title]",$product['title'],$show);
            $show = str_replace("[var.theme]",$theme,$show);
            if ($product[photopath] == ""){ $photopath = "themes/$theme/graphics/noimg-l.png"; } else { $photopath = $product['photopath']; }
            $show = preg_replace('/\[product.photopath\b[^ ]*]/', $photopath, $show);
            
            $showthis .= "$show\n";
        }
    
        if (isset($showthis)){ echo $showthis; }
    }
    
    function ExecuteReduceStock($orderid){
        $minimumstock = mysql_query("SELECT minimumstock FROM settings");
        $minimumstock = mysql_result($minimumstock, 0);
    
        $fetchordered = mysql_query("SELECT * FROM ordered WHERE orderid='$orderid' ");
        while ($ordered = mysql_fetch_array($fetchordered)){
            $variationQuan = mysql_query("SELECT value FROM fields WHERE id='$ordered[variationid]' ");
            $variationQuan = mysql_result($variationQuan, 0); $newQuan = $variationQuan -1;
            $reduceQuan = mysql_query("UPDATE fields SET value='$newQuan' WHERE id='$ordered[variationid]'") or die(mysql_error());
            
            if ($newQuan <= $minimumstock) { $this->ExecuteLowStock($ordered['itemid'], $ordered['variationid']); }
        }
    }
    
    function ExecuteProcessOrder($orderid, $orderstatus){
    
        $settings = mysql_query("SELECT * FROM settings LIMIT 1");
        $settings = mysql_fetch_array($settings);
        $websiteurl = $settings['websiteurl'];
        $domainname = str_replace("http://", "", "$websiteurl");
        $domainchunks = explode("/", $domainname);
        $domainname = $domainchunks[0];
        $sitename = $settings['businessname'];
        $conversions = $settings['conversions'];
        $minimumstock = $settings['minimumstock'];
        $email = $settings[email];
        $sendas = "donotreply@$domainname";
    
        if ($orderstatus == "processing"){
            //------> 1/3 send emails
            $fetchorder = mysql_query("SELECT * FROM orders WHERE id='$orderid'");
            $order = mysql_fetch_array($fetchorder);
            $custemail = $order['email']; $adminemail = $settings['email'];
            $subject = $order['emailsubject'];
            $emailcontent = $order['emailcontent'];
            $headers      = "From: $sitename <$sendas>" . "\r\n";      
              $headers .= 'MIME-Version: 1.0' . "\r\n";      
              $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
            $emailtask = mail($custemail, $subject, $emailcontent, $headers);
            $emailtask = mail($adminemail, $subject, $emailcontent, $headers);
                  /* debug */ if ($debug == "yes") { echo "<hr>$custemail // $email // $subject // $headers // $sendas"; }
            //------> 2/3 database query
            $makeprocessed = mysql_query("UPDATE orders SET status='processed' WHERE id='$orderid'") or die(mysql_error());
            //------> 3/3 conversions
            echo "$conversions";
            ///////////////////////////////////////
            ///////////////////////////////////////
        } elseif ($payment != "Completed" && !empty($_POST) ) {
            // If 'INVALID', send an email. TODO: Log for manual investigation. 
            foreach ($_POST as $key => $value){ 
            $emailtext .= $key . " = " .$value ."\n\n"; 
            }
            $emailtext = "Below are the records of a faulty PayPal transaction, regarding $businessname\n\n".$emailtext; 
            mail($email, "Live-INVALID IPN from $sitename", $emailtext . "\n\n" . $req); 
        }
    }
    
    function ExecuteLowStock($itemid, $variationid){
      $getsettings = mysql_query("SELECT * FROM settings") or die(mysql_error());
      $getsettings = mysql_fetch_array($getsettings);
      $websiteurl = $getsettings['websiteurl'];
      $adminurl = $websiteurl."/"."admin";

      $to      = $getsettings['emailtoalert'];
      $subject = 'Cartcake --> Limited stock';

      $getitem = mysql_query("SELECT * FROM items
      WHERE id='$itemid'") or die(mysql_error());
      $getitem = mysql_fetch_array($getitem);
      
      $getvariation = mysql_query("SELECT * FROM fields
      WHERE id='$itemid'") or die(mysql_error());
      $getvariation = mysql_fetch_array($getvariation);

      $message = "This is Cartcake. <br>\n 
      The item called <b>$getitem[title]</b> <i>$getvariation[attribute]</i> has limited stock. <br>\n 
      Use the link below to update the stock. <br>\n
      <a href=\"$adminurl\" target=\"_blank\">$adminurl</a><br>\n";
      $headers = "From: Cartcake <donotreply@$domainname><br>" . "\r\n";
      "Reply-To: donotreply@$domainname" . "\r\n" .
      "mime-version: 1.0" . "\r\n" .
      "content-type: text/html; charset=utf8" . "\r\n";
      "X-Mailer: PHP/" . phpversion();
      $sendEmail = @mail($to, $subject, $message, $headers);
      if ($sendEmail === true) { return true; }
    }
    
    function BeforeDownload($orderedid){
        $hook_BeforeDownload = "yes";
        $fetchplugins = mysql_query("SELECT * FROM plugins WHERE active='yes' LIKE '%beforeDownload%' ");
        while ($plugin = mysql_fetch_array($fetchplugins)){
            require("$plugin[file]".".php");
        }
    }
    
    function AfterDownload($orderedid){
        $hook_AfterDownload = "yes";
        $fetchplugins = mysql_query("SELECT * FROM plugins WHERE active='yes' LIKE '%afterDownload%' ");
        while ($plugin = mysql_fetch_array($fetchplugins)){
            require("$plugin[file]".".php");
        }
    }

}

?>